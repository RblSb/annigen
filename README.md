Генератор форумных тем конкурсов.
Для сборки нужен [Haxe 4.3+](https://haxe.org/download) или новее.

Для запуска веб сервера из вскода нужен NodeJS и:
```
npm i light-server -g
```

Сборка через:
```hxml
haxe build.hxml
```

Дебаг-режим с картами исходников:
```hxml
haxe build.hxml --debug
```
