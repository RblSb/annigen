package;

import haxe.macro.Expr;
import haxe.macro.Context;

class Macro {

	/*public static macro function includeFile2( fileName : Expr ) {
		var str = switch( fileName.expr ) {
		case EConst(c):
			switch( c ) {
				case CString(str): str;
				default: null;
			}
			default: null;
		}
		if( str == null ) Context.error("Should be a constant string", fileName.pos);
		var f = try sys.io.File.getContent(Context.resolvePath(str)) catch( e : Dynamic ) Context.error(Std.string(e), fileName.pos);
		return macro $v{f};
	}*/

	public static macro function includeFile(path:String):ExprOf<String> {
		var f = try {
			sys.io.File.getContent(Context.resolvePath(path));
		} catch(e:Dynamic) {
			Context.error(Std.string(e), Context.currentPos());
		}
		return macro $v{f};
	}

	public static macro function includeFolderFiles(folder:String, path:String):ExprOf<Array<String>> {
		var f = function() {
			var types = sys.io.File.getContent(Context.resolvePath(path));
			var arr = types.split("\n");
			var list:Array<String> = [];
			for (type in arr) {
				// trace(Context.resolvePath("й й"));
				// trace(type);
				// if (type == "Музыкальный конкурс") continue;
				var data = sys.io.File.getContent(Context.resolvePath(folder+'/${type}.txt'));
				list.push(data);
			}
			return list;
		}
		return macro $v{f()};
	}

}
