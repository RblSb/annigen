package;

import js.html.TextAreaElement;
import js.html.SelectElement;
import js.html.InputElement;
import js.Browser.document;
import js.Browser.window;
import haxe.Template;

class Main {

	var typesText = Macro.includeFile("../res/types.txt");
	var mainTopic = Macro.includeFile("../res/main.txt");
	var worksTopic = Macro.includeFile("../res/works.txt");
	var jurysTopic = Macro.includeFile("../res/jurys.txt");
	var rules = Macro.includeFolderFiles("../res/rules", "../res/types.txt");
	var works = Macro.includeFolderFiles("../res/works", "../res/types.txt");
	var jurys = Macro.includeFolderFiles("../res/jurys", "../res/types.txt");
	var types:Array<String>;

	static function main():Void {
		document.addEventListener("DOMContentLoaded", function(e) {
			new Main();
		});
	}

	function new():Void {
		types = typesText.split("\n");

		var select = cast(document.getElementById("type"), SelectElement);
		for (type in types) {
			var option = document.createOptionElement();
			option.text = type;
			select.add(option);
		}

		var btn = document.getElementById("generate");
		btn.onclick = function(e) {
			generate();
			e.preventDefault();
			return false;
		}
		btn.onsubmit = function(e) {
			e.preventDefault();
			return false;
		}
	}

	function generate():Void {
		document.getElementById("output").innerHTML = "";
		var typeEl = cast(document.getElementById("type"), SelectElement);
		var typeId = typeEl.selectedIndex;
		var type = typeEl.value;
		var name = type + " " + Date.now().getFullYear();

		var sets = {
			year: 'numeric',
			month: 'long',
			day: 'numeric'
		};
		var entryEnd = getDate("entryEnd", sets);
		var contestEnd = getDate("contestEnd", sets);

		var sets = {
			name: name,
			prizes: makePrizes(),
			entryEnd: entryEnd,
			contestEnd: contestEnd,
			judgingEnd: contestEnd,
			type_rules: rules[typeId],
			type_works_rules: works[typeId],
			type_rating_rules: jurys[typeId],
		};
		sets.type_rules = new Template(sets.type_rules).execute(sets);
		sets.type_works_rules = new Template(sets.type_works_rules).execute(sets);
		sets.type_rating_rules = new Template(sets.type_rating_rules).execute(sets);

		var mainText = new Template(mainTopic).execute(sets);
		var worksText = new Template(worksTopic).execute(sets);
		var jurysText = new Template(jurysTopic).execute(sets);

		addTitle(name);
		addForm(mainText);
		if (type != "Конкурс рассказов" && type != "Конкурс статей") {
			addTitle(name + " — Работы");
			addForm(worksText);
		}
		addTitle(name + " — Оценка");
		addForm(jurysText);
	}

	inline function getDate(id:String, sets):String {
		var d = cast(document.getElementById(id), InputElement).value;
		return (Date.fromString(d):Dynamic).toLocaleString("ru", sets);
	}

	function makePrizes():String {
		var text = "";
		var len = 6;
		for (i in 1...len) {
			var bin = cast(document.getElementById("bin" + i), InputElement);
			if (bin.value != "") text += '$i-е место: ${bin.value} бинов';
			var money = cast(document.getElementById("money" + i), InputElement);
			if (money.value != "") text += ' + ${money.value} рублей';
			if (bin.value != "" || money.value != "") {
				if (i != len-1) text += "\n";
			}
		}
		return text;
	}

	function addTitle(text:String):Void {
		var out = document.getElementById("output");
		var div = document.createElement("h3");
		div.innerHTML = text;
		out.appendChild(div);
	}

	function addForm(text:String):Void {
		var out = document.getElementById("output");
		var div = document.createTextAreaElement();
		div.value = text;
		div.rows = 7;
		div.cols = 50;
		out.appendChild(div);
	}

}
