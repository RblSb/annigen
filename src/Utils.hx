package;

import js.html.XMLHttpRequest;
import haxe.Json;

class Utils {
	
	public static var console = Console;
	
	public static function loadJSON(path, callback=null) {
		var xhr = new XMLHttpRequest();
		xhr.open("GET", path, true);
		xhr.onload = function() {
			if (callback != null) callback(Json.parse(xhr.responseText));
		}
		xhr.send();
	}
	
}

class Color {
	public static function toInt(rgb:String):Array<Int> {
		var data = rgb.substring(rgb.indexOf("(")+1, rgb.indexOf(")"));
		var colors = data.split(",");
		var r = Std.parseInt(colors[0]);
		var g = Std.parseInt(colors[1]);
		var b = Std.parseInt(colors[2]);
		return [r, g, b];
	}
	
	public static function toRGB(c:Array<Int>):String {
		return "rgb("+c[0]+","+c[1]+","+c[2]+")";
	}
}

class Console {
	
	public static function log(?a:Any, ?a2:Any, ?a3:Any):Void {
		if (a3 != null) trace(a, a2, a3);
		else if (a2 != null) trace(a, a2);
		else trace(a);
	}
	
	public static function clear(?args):Void {
		#if js js.Browser.console.clear(); #end
	}
}